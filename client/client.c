#include <sys/stat.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <dirent.h>

// ! Make sure it has the same port with client.c
#define PORT 8080

char arguments[2048] = {0}, root[4] = "0";

void removeChar(char * str, char charToRemmove){
    int i, j;
    int len = strlen(str);
    for(i=0; i<len; i++) {
        if(str[i] == charToRemmove) {
            for(j=i; j<len; j++) str[j] = str[j+1];
            len--;
            i--;
        }
    }
}

int checkArguments(int sock){
	char *semicol = strstr(arguments, ";"); // ! CHECKING SEMICOLON EXISTENCE
	if(!semicol) {
		write(sock, "\0", 1024);
		printf("ERROR: EXPECTED ';' AT THE END OF INPUT\n");
		return 0;
	}

	if(strlen(semicol) > 1) { // ! CHECKING ARGUMENT AFTER SEMICOLON
		char i = 1;
		while(i < strlen(semicol)) {
			if(semicol[i] != ' ' && semicol[i] != '\n') {
				write(sock, "\0", 1024);
				char *un_used = &semicol[i];
				un_used[strlen(un_used) - 1] = '\0';
				printf("ERROR: UN-USED ARGUMENT '%s'\n", un_used);
				return 0;
			}
			i++;
		}
	}
	semicol[0] = '\0';

	if(arguments[strlen(arguments) - 1] == ' ') {
		printf("ERROR: SEMICOLON ';' MUST NOT SEPARATED WITH THE LAST ARGUMENT\n");
		write(sock, "\0", 1024);
		return 0;
	}
	return 1;
}

// ! ERROR HANDLING
// ? -> Uppercase
int upper(char c) { 
	if( c >= 'A' && c <= 'Z') return 1;
	return 0;
}

// ? -> Lowercase
int lower(char c) { 
	if( c >= 'a' && c <= 'z') return 1;
	return 0;
}

// ? -> Numeric
int numeric(char c) { 
	if( c >= '0' && c <= '9') return 1;
	return 0;
}

// ? -> Wrong Format
int check_unic_char(int sock, char *temp){
	int i = 0;
	for(i = 0; i < strlen(temp); i++){
		if( !upper(temp[i]) && !lower(temp[i]) && !numeric(temp[i]) ){
			printf("ERROR: WRONG FORMAT '%s'\nERROR DETAIL: USE UPPERCASE, LOWERCASE, OR NUMBER\n", temp);
			write(sock, "\0", 1024);
			return 1;
		}
	}
	return 0;
}

// ! Arguments After
int argumentsAfter(int sock, char *argument, int *i){
	int j = 0;
	char temp[1024] = {0};
	
	if((*i) + 1 >= strlen(arguments)) {
		printf("ERROR: TOO FEW ARGUMEN\n");
		write(sock, "\0", 1024);
		return 0;
	}
	(*i)++;
	while((*i) < strlen(arguments) && arguments[(*i)] != ' '){
		temp[j] = arguments[(*i)];
		j++;
		(*i)++;
	}
	if(!strlen(temp)) {
		printf("ERROR: TOO MANY SPACES\n");
		printf("ERROR DETAIL: EACH ARGUMENT MUST SEPARATED WITH 1 SPACES\n");
		write(sock, "\0", 1024);
		return 0;
	}
	strcpy(argument, temp);
	return 1;
}

// ! Unknown Command
void unknownCommand(int sock, char *arg) { 
	printf("ERROR: COMMAND '%s' NOT RECOGNIZED\n", arg);
	write(sock, "\0", 1024);
}

// ! Too Much Argument
int tooMuchArgument(int sock, int bookmark){
	if(bookmark < strlen(arguments)) {
		char *un_used = &arguments[bookmark];
		printf("ERROR: TOO MUCH ARGUMENT\n");
		printf("ERROR DETAIL: UN-USED ARGUMENT '%s'\n", un_used);
		write(sock, "\0", 1024);
		return 1;
	}
	return 0;
}

// ! Login
int login(int sock, char username[1024], char password[1024]) {
	char status[200] = {0};
	write(sock, username, 1024);
	write(sock, password, 1024);
	read(sock, status, 200);
	printf("%s\n", status);
	
	if(!strcmp(status, "ACCESS DENIED"))
		return 0;
		
	return 1;
}

// ! Root Grant Permission
void root_grant_permit(int sock){
	char arg[64] = {0}, db_name[1024] = {0}, username[1024] = {0};
	int bookmark = 0;
	char db_avail[64] = {0}, user_avail[64] = {0}, permit_granted[64] = {0};

	if(!argumentsAfter(sock, arg, &bookmark)) // ? Check Argument
		return;
	if(strcmp(arg, "PERMISSION")) {
		unknownCommand(sock, arg); return;
	}

	if(!argumentsAfter(sock, db_name, &bookmark)) return; // ? Check Database
	if(check_unic_char(sock, db_name)) return;
	write(sock, db_name, 1024);

	if(!argumentsAfter(sock, arg, &bookmark)) return; // ? Check Other Argument
	if(strcmp(arg, "INTO")) { 
		unknownCommand(sock, arg);
		return;
	}
	
	if(!argumentsAfter(sock, username, &bookmark)) return; // ? Get Username
	if(check_unic_char(sock, username)) return;

	if(tooMuchArgument(sock, bookmark)) return; // ? Check Many Arguments

	if(root[0] == '0') { // ? Checking if Client isn't Root
		printf("ERROR: AUTHORITY NOT ENOUGH. MUST LOGIN VIA ROOT USER\n");
		write(sock, "\0", 1024);
		return;
	}

	write(sock, username, 1024); // ? Checking if Client is Root

	read(sock, db_avail, 64); // ? Checking Database
	if(!strlen(db_avail)) {
		printf("ERROR: DATABASE NOT FOUND\n"); return;
	} else printf("%s\n", db_avail);

	read(sock, user_avail, 64); // ? Checking User
	if(!strlen(user_avail)) {
		printf("ERROR: USERNAME NOT FOUND\n"); return;
	} else printf("%s\n", user_avail);

	read(sock, permit_granted, 64); // ? Checking Permission to Database
	if(!strlen(permit_granted)) {
		printf("ERROR: USER '%s' ALREADY HAVE PERMISSION TO OPEN DB '%s'\n", username, db_name);
		return;
	} else printf("%s\n", permit_granted);
}

// ! Use Database
void use(int sock){
	int bookmark = 0;
	char db_name[1024] = {0};
	if(!argumentsAfter(sock, db_name, &bookmark)) return;
	if(check_unic_char(sock, db_name)) return;
	if(tooMuchArgument(sock, bookmark)) return;
	write(sock, db_name, 1024);

	char error[64] = {0};
	read(sock, error, 64);

	if(strlen(error)) {
		printf("%s\n", error);
		return;
	}
	printf("DATABASE ACCESSED\n");
}

// ! Create New
void create(int sock){
	char mode[64] = {0};
	int bookmark = 0;
	
	if(!argumentsAfter(sock, mode, &bookmark)) return;
	write(sock, mode, 64);
	
	// ! New User
	if(!strcmp(mode, "USER") && !strcmp(root, "1")) {
		char new_user[1024] = {0}, new_pwd[1024] = {0}, temp[32] = {0};
		char success[4] = {0};
		
		if(!argumentsAfter(sock, new_user, &bookmark)) return;
		if(check_unic_char(sock, new_user)) return;
		write(sock, new_user, 1024);
		if(!argumentsAfter(sock, temp, &bookmark)) return;
		if(strcmp(temp, "IDENTIFIED")) {
			printf("new user: %s\n", new_user);
			unknownCommand(sock, temp);
			return;
		}
		if(!argumentsAfter(sock, temp, &bookmark)) return;
		if(strcmp(temp, "BY")) {
			unknownCommand(sock, temp);
			return;
		}
		if(!argumentsAfter(sock, new_pwd, &bookmark)) return;
		if(check_unic_char(sock, new_pwd)) return;
		if(tooMuchArgument(sock, bookmark)) return;

		write(sock, new_pwd, 1024);
		read(sock, success, 4);
		
		if(success[0] == '1') printf("USER CREATED\n");
		else printf("ERROR: USERNAME EXIST\n");
	}

	// ! New Database
	else if(!strcmp(mode, "DATABASE")) {
		char dir_name[512] = {0};
		char success[4] = {0};

		if(!argumentsAfter(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		if(tooMuchArgument(sock, bookmark)) return;
		
		write(sock, dir_name, 1024);
		read(sock, success, 4);
		
		if(success[0] == '1') printf("DATABASE CREATED\n");
		else printf("ERROR: NAME EXIST\n");
	}

	// ! New Table
	else if(!strcmp(mode, "TABLE")) {
		char dir_name[512] = {0};
		char* util;
		char success[4] = {0};

		removeChar(arguments, '(');
		removeChar(arguments, ')');
		removeChar(arguments, ',');

		if(!argumentsAfter(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;

		write(sock, dir_name, 1024);

		if(!argumentsAfter(sock, dir_name, &bookmark)) return;

		util = strstr(arguments, dir_name);
		write(sock, util, strlen(util));
		read(sock, success, 4);

		if(success[0] == '1') printf("TABLE CREATED\n");
		else if(success[0] == '3') printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
		else if(success[0] == '2') printf("ERROR: UNDEFINED DATATYPE\n");
		else printf("ERROR: TABLE EXIST\n");
	}
        else printf("ERROR: COMMAND '%s' IS NOT RECOGNIZED\n", mode);
}

// ! Drop
void drop(int sock){
    char mode[512] = {0};
	int bookmark = 0;
	
	if(!argumentsAfter(sock, mode, &bookmark)) return;
	if(check_unic_char(sock, mode)) return;
	
	write(sock, mode, 1024);
	
	// ! Drop User
	if(!strcmp(mode, "DATABASE")) {
		char dir_name[512] = {0};
		char success[4] = {0};
		
		if(!argumentsAfter(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		if(tooMuchArgument(sock, bookmark)) return;
		
		write(sock, dir_name, 1024);
		read(sock, success, 4);

		if(success[0] == '1') printf("DATABASE DELETED\n");
		else if(success[0] == '0') printf("ERROR: DATABASE DO NOT EXIST\n");
		else printf("ERROR : YOU DO NOT HAVE ACCESS!!\n");
		return;
	} else if(!strcmp(mode, "TABLE")) {
		char dir_name[512] = {0};
		char success[4] = {0};

		if(!argumentsAfter(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		if(tooMuchArgument(sock, bookmark)) return;

		write(sock, dir_name, 1024);
		read(sock, success, 4);

		if(success[0] == '1') printf("TABLE DELETED\n");
		else if(success[0] == '0') printf("ERROR: TABLE DO NOT EXIST\n");
		else if(success[0] == '3') printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
		else printf("TABLE IS NOT REMOVED\n");

	}
	else if(!strcmp(mode, "COLUMN")){
		char dir_name[512] = {0};
		char success[4] = {0};

		if(!argumentsAfter(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;

		printf("%s-a\n", dir_name);
		write(sock, dir_name, 1024);

		if(!argumentsAfter(sock, dir_name, &bookmark)) return;

		printf("%s-a\n", dir_name);

		if(strcmp(dir_name, "FROM")){
			printf("WRONG SYNTAX!\n");
			return;
		}
		if(!argumentsAfter(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;

		printf("%s-a\n", dir_name);
		write(sock, dir_name, 1024);
		read(sock, success, 4);

		if(success[0] == '0') printf("TABLE DOESN'T EXIST\n");
		else if(success[0] == '1') printf("SUCCESSFULLY DELETED\n");
		else printf("COLUMN DOESN'T EXIST\n");
	}
	else printf("'%s' NOT RECOGNIZED. DO YOU MEAN 'USER', 'DATABASE' OR 'TABLE'?\n", mode);
}

// ! Insert
void insert(int sock){
	char mode[512] = {0};
	char success[4] = {0};
	char avail[4] = {0};
	char* val;
	int bookmark = 0;
	if(!argumentsAfter(sock, mode, &bookmark)) return;
	if(check_unic_char(sock, mode)) return;
	printf("%s\n", mode);
	write(sock, mode, 512);
	if(!strcmp(mode, "INTO")){
		char dir_name[1024];
		read(sock, avail, 4);
		printf("nih sudah read success %c\n", success[0]);
		if(avail[0] == '1'){
			printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
			return;
		}
		if(!argumentsAfter(sock, dir_name, &bookmark)) return;
		if(check_unic_char(sock, dir_name)) return;
		write(sock, dir_name, 1024);
		if(!argumentsAfter(sock, dir_name, &bookmark)) return;
		printf("%s %s\n", arguments, dir_name);
		removeChar(arguments, ',');
		removeChar(arguments, '(');
		removeChar(arguments, ')');
		removeChar(arguments, 39);
		removeChar(dir_name, '(');
		removeChar(dir_name, ',');
		removeChar(dir_name, 39);
		printf("%s %s\n", arguments, dir_name);
		val = strstr(arguments, dir_name);
		printf("%s\n", val);
		write(sock, val, 1024);
		read(sock, success, 4);
		if(success[0] == '0') printf("TABLE DOESN'T EXIST\n");
		else printf("SUCCESSFULLY INSERTED\n");
	}
	else printf("'%s' NOT RECOGNIZED. DO YOU MEAN 'INTO'?\n", mode);
}

// ! Where
// ? Suggestion
void where_sug() {
	printf("SUGGESTION 1: WHERE [column_name]='value' FOR STRING\nEXAMPLE 1: WHERE name='[name]'\nSUGGESTION 2: WHERE [column_name]=value FOR INT\nEXAMPLE 2: WHERE age=[age_number]\n\n");
}

// ? Check Where with Format => column1:valu1:datatype
int check_where(int sock, char *where_arg){
	char *value = strstr(where_arg, "=");
	if(!value) {
		printf("ERROR: UNKNOWN FORMAT\n");
		where_sug();
		write(sock, "\0", 1024);
		return 0;
	}
	char temp[1024] = {0}, temp2[1024] = {0};

	strcpy(temp2, value);
	value[0] = '\0';
	strcpy(temp, where_arg);

	if(check_unic_char(sock, temp)) return 0;

	// ? Check Value
	// ? String Value :
	char c = '\'', dataType[16] = {0};
	if(temp2[1] == c && temp2[strlen(temp2) - 1] == c) {
		temp2[strlen(temp2) - 1] = '\0';
		char *temp3 = &temp2[2];
		if(check_unic_char(sock, temp3)) {
			where_sug();
			return 0;
		}
		strcpy(temp2, temp3);
		strcpy(dataType, "string");
	}
	// ? Integer Value :
	else {
		for(int i = 1; i < strlen(temp2); i++) {
			if(!numeric(temp2[i])) {
				printf("ERROR: UNRECOGNIZE FORMAT\n");
				where_sug();
				write(sock, "\0", 1024);
				return 0;
			}
		}
		char *temp3 = &temp2[1];
		strcpy(temp2, temp3);
		strcpy(dataType, "int");
	}

	strcpy(where_arg, temp);
	strcat(where_arg, ":");
	strcat(where_arg, temp2);
	strcat(where_arg, ":");
	strcat(where_arg, dataType);
	return 1;
}
int is_where (int sock, int *bookmark){
	char arg[1024] = {0};
	if((*bookmark) < strlen(arguments)) {
		
		// ? Check Where command
		write(sock, "WHERE", 1024);
		if(!argumentsAfter(sock, arg, bookmark)) return 0;
		if(strcmp(arg, "WHERE")) {
			unknownCommand(sock, arg);
			printf("DO YOU MEAN 'WHERE'?\n");
			write(sock, "\0", 1024);
			return 0;
		}
		write(sock, arg, 1024);
		if(!argumentsAfter(sock, arg, bookmark)) return 0;
		if(tooMuchArgument(sock, (*bookmark))) return 0;
		if(!check_where(sock, arg)) return 0;
	}
	write(sock, arg, 1024);
	return 1;
}

// ! SELECT
void sel(int sock){
	int bookmark = 0, loop = 1, i = 1;
	char arg[1024] = {0}, table_name[1024] = {0}, header[2048] = {0};
	while(loop) {
		if(!argumentsAfter(sock, arg, &bookmark)) return;

		//select star
		if(i == 1 && !strcmp(arg, "*")) {
			write(sock, arg, 1024);
			break;
		}

		if(arg[strlen(arg) - 1] != ',') loop = 0;
		else arg[strlen(arg) - 1] = '\0';

		//check select star
		if(check_unic_char(sock, arg)) return;

		// ? Command Executed
		strcat(header, arg); strcat(header, ":");
		write(sock, arg, 1024);
	}

	// ? Telling Server to stop
	write(sock, "-", 1024);
	if(!argumentsAfter(sock, arg, &bookmark)) return;
	if(strcmp(arg, "FROM")) {unknownCommand(sock, arg); return;}
	if(!argumentsAfter(sock, table_name, &bookmark)) return;
	if(check_unic_char(sock, table_name)) return;
	write(sock, table_name, 1024);
	if(!is_where(sock, &bookmark)) return;

	// ? Error Check
	char status[256] = {0};
	read(sock, status, 256);
	if(strlen(status)) {
		printf("%s\n", status); 
		return;
	}

	// ? Check Table
	read(sock, status, 256);
	if(strlen(status)) {
		printf("%s\n", status);
		return;
	}

	// ? Check Column
	read(sock, status, 256);
	while(strlen(status)) {
		printf("ERROR: COLUMN '%s' NOT RECOGNIZED\n", status);
		read(sock, status, 256);
		if(!strlen(status)) return;
	}

	// ? Display data
	loop = 1; 
	char data[2048];

	header[strlen(header) - 1] = '\0';
	printf("%s\n", header);
	while(loop) {
		read(sock, data, 2048);
		printf("%s\n", data);
		if(!strlen(data)) break;
	}
}

int main(int argc, char const *argv[]) {
	struct sockaddr_in address;
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	printf("Request a connection to server...");
	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
		return -1;
	} else {
		puts("Connection established\n");
		puts("You will access the server when no one is currently accessing it\nPlease wait...\n");
		sleep(3);
		char permission[4] = {0};
		read(sock, permission, 4);
		
		strcpy(root, "0");
		if(argc < 2) strcpy(root, "1");
		write(sock, root, 4);

		char username[1024] = {0}, password[1024] = {0};
		strcpy(username, argv[2]);
		strcpy(password, argv[4]);
		if(!strcmp(root, "1") || login(sock, username, password) == 1) {
			printf("Welcome!\nType 'exit;' to exit the program\n");
			while(true) {
				char com[1024] = {0};
				scanf("%s", com);
				fgets(arguments, 2048, stdin);

				if(com[ strlen(com)-1 ] != ';' && !checkArguments(sock)) continue;
				if(com[ strlen(com)-1 ] == ';') com[ strlen(com)-1 ] = '\0';
				write(sock, com, 1024);

				if(!strcmp(com, "CREATE")) create(sock);
				else if(!strcmp(com, "USE")) use(sock);
				else if(!strcmp(com, "GRANT") && !strcmp(root, "1")) root_grant_permit(sock);
				else if(!strcmp(com, "DROP")) drop(sock);
				else if(!strcmp(com, "INSERT")) insert(sock);
				else if(!strcmp(com, "SELECT")) sel(sock);
				else if(!strcmp(com, "exit")) {
					printf("Program Ending\n");
					break;
				}
				else {printf("ERROR: COMMAND '%s' NOT RECOGNIZED\n", com); continue;}
			}
		} else printf("Program Ending\n");
	}

	return 0;
}
