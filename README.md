# sisop-praktikum-fp-2023-BJ-U08


### Group Members


| Name | NRP |
| --- | --- |
| Mochammad Naufal Ihza Syahzada  | 5025211260 |
| Ariel Pratama Menlolo | 5025211194 |
| Teuku Aulia Azhar | 5025201142 |

# **Main File Explanation**
## Client ("/client/client.c")

```c
int main(int argc, char const *argv[]) {
	struct sockaddr_in address;
	int sock = 0, valread;
	struct sockaddr_in serv_addr;
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	memset(&serv_addr, '0', sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
		printf("\nInvalid address/ Address not supported \n");
		return -1;
	}

	printf("Request a connection to server...");
	if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
		printf("\nConnection Failed \n");
		return -1;
	} else {
		puts("Connection established\n");
		puts("You will access the server when no one is currently accessing it\nPlease wait...\n");
		sleep(3);
		char permission[4] = {0};
		read(sock, permission, 4);
		
		strcpy(root, "0");
		if(argc < 2) strcpy(root, "1");
		write(sock, root, 4);

		char username[1024] = {0}, password[1024] = {0};
		strcpy(username, argv[2]);
		strcpy(password, argv[4]);
		if(!strcmp(root, "1") || login(sock, username, password) == 1) {
			printf("Welcome!\nType 'exit;' to exit the program\n");
			while(true) {
				char com[1024] = {0};
				scanf("%s", com);
				fgets(arguments, 2048, stdin);

				if(com[ strlen(com)-1 ] != ';' && !checkArguments(sock)) continue;
				if(com[ strlen(com)-1 ] == ';') com[ strlen(com)-1 ] = '\0';
				write(sock, com, 1024);

				if(!strcmp(com, "CREATE")) create(sock);
				else if(!strcmp(com, "USE")) use(sock);
				else if(!strcmp(com, "GRANT") && !strcmp(root, "1")) root_grant_permit(sock);
				else if(!strcmp(com, "DROP")) drop(sock);
				else if(!strcmp(com, "INSERT")) insert(sock);
				else if(!strcmp(com, "SELECT")) sel(sock);
				else if(!strcmp(com, "exit")) {printf("See you later!\n"); break;}
				else {printf("ERROR: COMMAND '%s' NOT RECOGNIZED\n", com); continue;}
			}
		} else printf("See you later!\n");
	}

	return 0;
}
```

<details><summary>How the main function works:</summary>

This program establishes a client-side connection to a server using sockets. In the beginning, it creates a socket using the socket() function. If the socket creation fails, an error message is displayed, and the program exits with a return value of -1. The program then sets up the server address and port in the serv_addr structure. It attempts to establish a connection to the server using the connect() function. If the connection fails, an error message is displayed, and the program exits with a return value of -1. On a successful connection, a success message is printed. After a brief simulated delay of 3 seconds, the program reads a response from the server into the permission array. Based on command line arguments, it sets the root variable and sends it to the server using the write() function. Next, the program reads the username and password from the command line arguments and stores them in the respective arrays. If the root variable is "1" or the login process (using the login() function) is successful (not provided in the code), the program enters a loop to interact with the server. Within the loop, the program prompts the user for a command using scanf() and reads the corresponding arguments using fgets(). The command is sent to the server using the write() function.

Depending on the received command, the program performs various actions such as creating a database, using a database, granting permissions (if the root variable is "1"), dropping a database, inserting data, selecting data, or exiting the program. If the user enters "exit", the program breaks out of the loop and displays a farewell message. Otherwise, if the login process fails or the entered command is not recognized, appropriate messages are displayed. Finally, the program returns 0 to indicate successful execution.
</details>

**_NOTE : Before executing client program, make sure database program running!_**

## Database ("/database/database.c")

```c
int main(int argc, char const *argv[]) {
	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1, addrlen = sizeof(address);

	// ? Socket
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( PORT );

	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	if (listen(server_fd, 1) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	char temp[SET_SIZE] = {0};
	strcpy(temp, path);
	strcat(temp, "database/databases");
	if(!mkdir(temp, S_IRWXU)){
		strcat(temp, "/root_db");
		mkdir(temp, S_IRWXU);
	}

	// Connect With Client
	puts("Waiting for client...");
	while ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))) {
		printf("Connected to client\n");
		pthread_t client_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;
		if(pthread_create(&client_thread, NULL, connection_control, (void*) new_sock) < 0) {
			perror("Could not create thread");
			return 1;
		}
		pthread_join(client_thread, NULL);
		puts("Disconnected");
		puts("Waiting for client");
	}
	if(new_socket < 0){
		perror("accept");
		exit(EXIT_FAILURE);
	}

	return 0;
}
```

<details><summary>How the main function works:</summary>
First, it creates a socket using the socket() function and sets socket options to allow reusing the address and port using setsockopt(). The program then binds the socket to a specified address and port using bind(). If any of these steps fail, appropriate error messages are displayed, and the program exits. Next, the program starts listening for incoming connections using listen(). If the listening process fails, an error message is displayed, and the program exits. Afterward, the program creates a directory to store databases if it doesn't already exist using mkdir(). The directory path is constructed by concatenating strings. The program then enters a loop where it waits for client connections using accept(). Once a client connects, a new thread is created to handle the connection. The main thread waits for the connection handling thread to finish using pthread_join(). Afterward, it continues to wait for new client connections by returning to the top of the loop. If the accept() function fails, an error message is displayed, and the program exits.> Finally, the program returns 0 to indicate successful execution.
</details>

_**NOTE : This file need to be execute before client program run!**_

## Containerization
> For the containerization process we first make a Dockerfile so that the files can be made to an image to be ran.
```c
FROM ubuntu:latest

WORKDIR /app

COPY . /app

RUN apt-get update && apt-get install -y gcc

CMD gcc -o client ./client/client.c && \
    gcc -o database ./database/database.c && \
    ./client && \
    ./database && \
```
> Here the image install and updates gcc and then runs the c files by first accessing the directories the c files are in adn then compiling them. after they are ran using the ./ command to make this an image we run this file using the command "sudo docker build -t imagename" with the image name being whatever we want our image to be named. After we can upload to dockerhub using the the command "docker tag imgaeid username/storage-app" in this case the image id is the id for the image weve created and the user is our dockerhub username. then we can push using the docker push command "docker push username/storage-app". Now the question asks us to run 5 instnces of the image 5 times from specific folder names,so we make 5 different files named Sukolilo, Keputih, Gebang, Mulyos, dan Semolowaru respectively and inside every single file we put the necessary files for the image to run, then we make a folder named docker-compose.yml which has the following inside:

```c
version: "3"
services:
  Gebang:
    build: 
      context: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Gebang
      dockerfile: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Dockerfile
    deploy:
      replicas: 5
  Keputih:
    build:
      context: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Keputih
      dockerfile: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Dockerfile
    deploy:
      replicas: 5
    Mulyos:
    build:
      context: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Mulyos
      dockerfile: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Dockerfile
    deploy:
      replicas: 5
    Semolowaru:
    build:
      context: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Semolowaru
      dockerfile: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Dockerfile
    deploy:
      replicas: 5
    Sukolilo:
    build:
      context: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Sukolilo
      dockerfile: /home/brel/Documents/efpe/sisop-praktikum-fp-2023-bj-u08-main/Dockerfile
    deploy:
      replicas: 5
```
> Then all we need to do is run the command 'docker-compose up -d' and it will run each of the created folders 5 times

# **Requested Point Explanation**
### Autentikasi


```c
int login(int new_socket){
	char username[SET_SIZE] = {0}, password[SET_SIZE] = {0};
	read(new_socket, username, SET_SIZE);	puts(username);
	read(new_socket, password, SET_SIZE);	puts(password);

	FILE* fp = open_file("database/databases/users.txt");

	char temp[SET_SIZE] = {0}, db[SET_SIZE] = {0};
	int correct_pass = 0;
	strcat(temp, username);
	strcat(temp, ":");
	strcat(temp, password);
	while(fgets(db, sizeof(db), fp)){
		if(db[strlen(db)-1] == '\n') db[strlen(db)-1] = '\0';
		if(!strcmp(temp, db)) {
			correct_pass = 1;
			break;
		}
	}
	char status[200] = {0};
	if(correct_pass) {
		strcpy(status, "ACCESS GRANTED");
		strcpy(client, username);
	}
	else strcpy(status, "ACCESS DENIED");
	write(new_socket, status, 200);
	fclose(fp);
	if(strcmp(status, "ACCESS GRANTED") == 0) return 1;
	return 0;
}
```

> Function login handles the login process for a client connecting to the server. Within the function, the username and password provided by the client are read from the new_socket using the read() function. These credentials are stored in character arrays username and password, respectively. The function then opens a file named "database/databases/root_db/users.txt" using an undefined open_file() function, and it returns a FILE pointer to access the file. To authenticate the user, the function compares the client's credentials with the records stored in the users.txt file. It iterates over each line in the file using fgets() and checks if the current line matches the concatenated username and password stored in the temp array. If a match is found, the correct_pass flag is set to 1, indicating a correct password, and the loop is terminated.

> After the loop, a status message is prepared based on the result of the authentication process. If the correct_pass flag is set, the status is assigned as "ACCESS GRANTED," and the global variable client (whose definition is missing in the code) is set to the authenticated username. Otherwise, the status is set as "ACCESS DENIED." The function writes the status message to the client using the write() function, informing the client of the authentication result. The users.txt file is then closed using the fclose() function Finally, if the status is "ACCESS GRANTED," indicating a successful login, the function returns 1; otherwise, it returns 0.


- [x] Login & Password
- [x] Create User (root only)

![authentication proof](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/autentikasi.jpg)
![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/userstxt.jpg)

### Autorisasi

```c
// ! Use
void use(int new_socket){
	//delete previous path
	use_db_path[0] = '\0';

	char db_name[SET_SIZE] = {0};
	read(new_socket, db_name, SET_SIZE);
	if(!strlen(db_name)) printf("ERROR: IN USE FUNCTION\n");

	//if all going smoothly, send empty string

	//is db avail?
	if(!is_db_avail(db_name)) {
		printf("ERROR: DB NOT AVAIL\n");
		write(new_socket, "ERROR: DATABASE NOT FOUND", 64);
		return;
	}

	//is client root?
	if(!strlen(client)) {
		//client is root
		printf("CLIENT IS ROOT\n");
	}

	//if client not roor -> is permission avail?
	else if(!permit(client, db_name, 0)){
		printf("ERROR: PERMIT NOT AVAILABLE\n");
		write(new_socket, "ERROR: YOU DON'T HAVE PERMISSION TO ACCESS THIS DATABASE", 64);
		return;
	}

	strcpy(use_db_path, path);
	strcat(use_db_path, "database/databases/");
	strcat(use_db_path, db_name);
	strcat(use_db_path, "/");

	printf("%s\n", use_db_path);
	write(new_socket, "\0", 64);
}
```

> Function use handles the "USE" command for switching to a specific database. At the beginning of the function, the global variable use_db_path is cleared by assigning the null terminator to its first character. The function reads the name of the desired database from the client through the new_socket using the read() function. An error check is performed to ensure that the received database name has a non-zero length. If the length is zero, an error message is printed. Next, the availability of the specified database is checked using the is_db_avail() function, which is not provided in the code snippet. If the database is not available, an error message is printed, and an appropriate error message is sent back to the client using the write() function. If the database is available, further checks are conducted based on the user accessing the database. If the user is identified as the root (indicated by an empty client string), a message stating "CLIENT IS ROOT" is printed.
> For non-root users, the function verifies whether the user has permission to access the specified database using the permit() function, which is not provided in the code snippet. If the user lacks permission, an error message is printed, and an appropriate error message is sent back to the client using the write() function. If the user possesses the necessary permissions, the function constructs the database path by concatenating relevant strings. The constructed path is stored in the use_db_path global variable. Finally, the function prints the constructed database path and sends an empty string back to the client to indicate a successful database switch.

```c
void root_grant_permit(int new_socket){
	char db_name[SET_SIZE] = {0}, username[SET_SIZE] = {0};
	read(new_socket, db_name, SET_SIZE); if(!strlen(db_name)) {printf("Error dir read ke 1 root_grant\n"); return;}
	read(new_socket, username, SET_SIZE); if(!strlen(username)) {printf("Error di read read ke-2 root_grant\n");return;}

	//check db avail
	if(!is_db_avail(db_name)) {write(new_socket, "\0", 64); return;}
	else write(new_socket, "DATABASE FOUND", 64);

	//check user avail
	if(!is_usr_avail(username)) {write(new_socket, "\0", 64); return;}
	else write(new_socket, "USER FOUND", 64);

	int status = permit(username, db_name, 1);

	/// 1 -> Permission exist
	if(status == 1) {write(new_socket, "\0", 64); return;}

	/// 2 -> Permission created
	else if(status == 2) write(new_socket, "PERMISSION GRANTED\n", 64);
}
```

> This function responsible for granting permissions to a user for a specific database. The function reads the database name and username from the client through the new_socket, performs error checks to ensure non-zero length for both strings, and proceeds to check the availability of the database and user using the is_db_avail() and is_usr_avail() functions, respectively (not provided in the code). Messages indicating the status are sent back to the client. The function then calls the permit() function to grant permission to the user for the specified database and returns different status values: 1 if the permission already exists (an empty string is sent to the client), and 2 if the permission is successfully created (a message is sent to the client).

- [x] Use
- [x] Grant Permission

![autorization proof](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/autorisasi.jpg)


## DDL (Data Definition Language)

```c
void create(int new_socket){
	printf("Masuk create\n");
	char mode[64] = {0};
	read(new_socket, mode, 64);
	
	// ! User
	if(!strcmp(mode, "USER") && !strlen(client)) {
		char new_user[SET_SIZE] = {0}, new_pwd[SET_SIZE] = {0};
		read(new_socket, new_user, SET_SIZE);
		if(!strlen(new_user)) {
			printf("NEW USER WRONG INPUT\n");
			return;
		}
		read(new_socket, new_pwd, SET_SIZE); 
		if(!strlen(new_pwd)) {
			printf("NEW PWD WRONG INPUT\n");
			return;
		}

		char temp[SET_SIZE] = {0};

		// ? If username avail
		if(is_usr_avail(new_user)) write(new_socket, "0", 4);

		// ? Username not avail
		write(new_socket, "1", 4);
		FILE* fp = open_file("database/databases/users.txt");

		// ? Store username and password
		strcpy(temp, new_user);
		strcat(temp, ":");
		strcat(temp, new_pwd);

		if(n_new_user) {
			printf("%d print slash n\n", n_new_user);
			fprintf(fp, "\n");}

			printf("%s\n", temp);
			fprintf(fp, "%s", temp);
			printf("USER CREATED\n");

			n_new_user++;
			fclose(fp);
		}

		// ! DATABASE
		else if(!strcmp(mode, "DATABASE")) {
			char dir_name[512] = {0}, temp_path[SET_SIZE] = {0}, success[4] ={0};
			read(new_socket, dir_name, SET_SIZE);
			if(!strlen(dir_name)) {
				printf("DATABASE WRONG INPUT\n");
				return;
			}

			strcpy(temp_path, path);
			strcat(temp_path, "databases/");
			strcat(temp_path, dir_name);
			if(!mkdir(temp_path, S_IRWXU)) {
				success[0] = '1'; success[1] = '\0';
				printf("DATABASE CREATED\n");
				if(strlen(client)) permit(client, dir_name, 1);
			} else {
				success[0] = '0';
				success[1] = '\0';
				printf("NAME EXIST");
			}
			write(new_socket, success, 4);
		}

		// ! TABLE
		else if(!strcmp(mode, "TABLE")) {
        	char success[4] = {0};
        	if(!strlen(use_db_path)) {
				printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
				success[0] = '3'; success[1] = '\0';
				write(new_socket, success, 4);
				return;
			}
			char dir_name[512] = {0};
			read(new_socket, dir_name, SET_SIZE);
			if(!strlen(dir_name)) {
				printf("TABLE WRONG INPUT\n");
				return;
			}
			char temp_path[SET_SIZE] = {0};
			strcpy(temp_path, use_db_path);
			strcat(temp_path, dir_name);
			strcat(temp_path, ".txt");
			char util[SET_SIZE] = {0};
			read(new_socket, util, SET_SIZE);
			printf("util : %s\n", util);
			char col[SET_SIZE]= {0}, datatype[SET_SIZE] = {0}, temp_datatype[SET_SIZE] = {0};
			int type = 2, total = strlen(util);
			for(int k = 0; k < total; k++) {
				if(util[k] == ' ') {
					char c[2];
					c[1] = '\0'; c[0] = ':';
					if(type % 2 == 0) strcat(col, c);
					else {
						if(!strcmp(temp_datatype, "string") || !strcmp(temp_datatype, "int")){
							strcat(datatype, temp_datatype);
							memset(temp_datatype, 0, sizeof(temp_datatype));
							strcat(datatype, c);
						} else {
							success[0] = '2'; success[1] = '\0';
							printf("FAIL, WRONG DATATYPE\n");
							write(new_socket, success, 4);
							return;
						}
					}
					type += 1;
				} else {
					char c[2];
					c[1] = '\0'; c[0] = util[k];
					if(type % 2 == 0) strcat(col, c);
					else strcat(temp_datatype, c);
				}
			}
			total = strlen(col);
			strcat(datatype, temp_datatype);
			col[total-1] = '\0';
			FILE* fp;
			if(fp = fopen(temp_path, "r")){
				success[0] = '0'; success[1] = '\0';
				fclose(fp);
				printf("TABLE EXIST");
			} else {
				success[0] = '1'; success[1] = '\0';
				fp = fopen(temp_path, "a");
				fprintf(fp,"%s\n%s\n",col, datatype);
				fclose(fp);
				printf("TABLE CREATED\n");
			}
			write(new_socket, success, 4);
		}
	else printf("ERROR: WRONG INPUT\n");
}
```

> This funtion handles the creation of users, databases, and tables. The function starts by reading a mode string from the client through the new_socket and performs different actions based on the mode. If the mode is "USER" and the client is not currently logged in, the function reads the new username and password from the client and checks if the username is available. If the username is available, it writes "0" to the client, indicating availability, and stores the username and password in a file. Otherwise, it writes "1" to the client, indicating that the username is already taken. If the mode is "DATABASE," the function reads the desired database name from the client and attempts to create a directory with that name. If the directory creation is successful, it writes "1" to the client and, if the client is logged in, grants the client permission to access the new database. Otherwise, it writes "0" to the client, indicating that the database name already exists.
> If the mode is "TABLE" and a database has been selected using the "USE" command, the function reads the table name, utility string, column names, and data types from the client. It processes the utility string to extract column names and data types and checks for any unsupported data types. If all data types are valid, it creates a new table file in the selected database directory, writes the column names and data types to the file, and writes "1" to the client, indicating successful table creation. If the table file already exists, it writes "0" to the client, indicating that the table name is already taken.

```c
void drop(int new_socket){
	char mode[512] = {0};
	read(new_socket, mode, SET_SIZE);
	
	// ! Database
	if(!strcmp(mode, "DATABASE")) {
		char dir_name[512] = {0};
		read(new_socket, dir_name, SET_SIZE);
		if(!strlen(dir_name)) {printf("DATABASE WRONG INPUT\n"); return;}
		char temp_path[SET_SIZE] = {0};
		strcpy(temp_path, path);
		strcat(temp_path, "database/databases/");
		strcat(temp_path, dir_name);
		char success[4] ={0};
		if(!strlen(client)) printf("CLIENT IS ROOT\n");
		else if(!permit(client, dir_name, 0)) {
			printf("ERROR: PERMIT NOT AVAILABLE\n");
			success[0] = '2'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
		}
		DIR* dir = opendir(temp_path);
		if(dir) {
			remove_directory(temp_path);
			success[0] = '1'; success[1] = '\0';
			printf("DATABASE REMOVED\n");
			//add to db access right
		} else {
			success[0] = '0'; success[1] = '\0';
			printf("DATABASE DO NOT EXIST");
		}
		write(new_socket, success, 4);
	}

	// ! Table
	else if(!strcmp(mode, "TABLE")) {
		char dir_name[SET_SIZE] = {0};
		read(new_socket, dir_name, SET_SIZE);
		char success[4] ={0};
		if(!strlen(use_db_path)){
			printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
			success[0] = '3'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
		}
		if(!strlen(dir_name)) {
			printf("TABLE WRONG INPUT\n");
			return;
		}
		char temp_path[SET_SIZE] = {0};
		strcpy(temp_path, use_db_path);
		strcat(temp_path, dir_name);
		strcat(temp_path, ".txt");
		FILE* fp;
		if(fp = fopen(temp_path, "r")) {
			if(!remove(temp_path)) {
				success[0] = '1'; success[1] = '\0';
				printf("TABLE SUCCESSFULLY REMOVED");
			} else {
				success[0] = '2'; success[1] = '\0';
				printf("TABLE IS NOT REMOVED");
			}
			fclose(fp);
		} else {
			success[0] = '0'; success[1] = '\0';
			fclose(fp);
			printf("TABLE DOESN'T EXIST");
		}
		write(new_socket, success, 4);
	}

	// ! Column
	else if(!strcmp(mode, "COLUMN")){
		if(!strlen(use_db_path)) {
		    printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
		    return;
        	}
		char dir_name[512] = {0}, success[4] = {0}, col_name[512] = {0}, table_name[512] = {0};
		
		read(new_socket, col_name, SET_SIZE);
		printf("col_name : %s\n", col_name);
		read(new_socket, table_name, SET_SIZE);
		printf("table_name : %s\n", table_name);
		
		char temp_path[SET_SIZE] = {0};
		
		strcpy(temp_path, use_db_path);
		strcat(temp_path, table_name);
		strcat(temp_path, ".txt");
		printf("%s\n", temp_path);
		FILE* fp = fopen(temp_path, "r");
		if(!fp){
			printf("TABLE DOESN'T EXIST\n");
			success[0] = '0'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
		}
		char db[8192] = {0};
		fgets(db, sizeof(db), fp);
		printf("%s", db);
		char col[512] = {0}, total_fill[4096] = {0};
		int no_col = 1, temp_col = 1, bisa = 0;
		for(int i = 0; i < strlen(db); i++){
			char c[2];
			c[1] = '\0';
			if(db[i] == ':'){
				printf("%s\n", col);
				if(!strcmp(col,col_name)){
					bisa = 1;
					break;
				}
				else{
					memset(col, 0, sizeof(col));
					no_col += 1;
				}
			}
			else{
				c[0] = db[i];
				strcat(col, c);
			}
		}
		printf("%d no_col\n", no_col);
		if(!bisa){
			printf("COLUMN DOESN'T EXIST\n");
			success[0] = '2'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
		}
		char c[2];
		c[1] = '\0';
		printf("%lu strlendb\n", strlen(db));
		for(int i = 0; i < strlen(db); i++){
			if(temp_col == no_col) {
				if(db[i] == ':') {
					temp_col += 1;
				}
				continue;
			} else {
				if(db[i] == ':') {
					temp_col += 1;
				}
				c[0] = db[i];
				strcat(total_fill, c);
			}
		}
		printf("total_fill : %s\n", total_fill);
		while(fgets(db, sizeof(db), fp)) {
			temp_col = 1;
			for(int i = 0; i < strlen(db); i++) {
				char c[2];
				c[1] = '\0';
				if(temp_col == no_col) {
					if(db[i] == ':') temp_col += 1;
					continue;
				} else {
					if(db[i] == ':') temp_col += 1;
					c[0] = db[i];
					strcat(total_fill, c);
				}
			}
		}
		printf("total_fill 2nd : %s\n", total_fill);
		fp = fopen(temp_path, "w");
		fprintf(fp, "%s", total_fill);
		fclose(fp);
		success[0] = '1'; success[1] = '\0';
		write(new_socket, success, 4);
	}
	else printf("WRONG INPUT\n");
}
```

> This handles the deletion of databases, tables, and columns. The function starts by reading a mode string from the client through the new_socket and performs different actions based on the mode. If the mode is "DATABASE," the function reads the database name from the client and attempts to remove the corresponding directory. If the removal is successful, it writes "1" to the client and prints a success message. If the client is logged in and doesn't have the necessary permission to delete the database, it writes "2" to the client and returns an error message. If the database directory doesn't exist, it writes "0" to the client and prints a corresponding message.
> If the mode is "TABLE," the function reads the table name from the client and attempts to remove the corresponding table file. If the removal is successful, it writes "1" to the client and prints a success message. If the table file doesn't exist, it writes "0" to the client and prints a corresponding message. If the mode is "COLUMN," the function reads the column name and table name from the client. It opens the table file, searches for the specified column name, and removes the column from each line of the table file. It then writes the modified table data back to the file, writes "1" to the client, and prints a success message. If the table file doesn't exist or the specified column name is not found, it writes "0" to the client and prints a corresponding message.

- [x] Create
- [x] Drop

![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/create.jpg)
![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/databaseandtablecreated.jpg)
![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/tabletxt.jpg)
![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/drop.jpg)

## DML (Data Manipulation Language)

```c
void insert(int new_socket){
	char mode[512] = {0};
	char success[4] = {0};
	char avail[4] = {0};
	read(new_socket, mode, 512);
	if(!strcmp(mode, "INTO")){
		if(!strlen(use_db_path)){
			printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
			avail[0] = '1'; avail[1] = '\0';
		} else {
			avail[0] = '0'; avail[1] = '\0';
		}
		write(new_socket, avail, 4);
		if(avail[0] == '1') return;
		char dir_name[SET_SIZE], temp_path[SET_SIZE];
		read(new_socket, dir_name, SET_SIZE);
		printf("%s\n", dir_name);
		char val[SET_SIZE];
		read(new_socket, val, SET_SIZE);
		printf("%s\n", val);
		strcpy(temp_path, use_db_path);
		strcat(temp_path, dir_name);
		strcat(temp_path, ".txt");
		printf("%s\n", temp_path);
		
		FILE* fp;
		if (fp = fopen(temp_path, "a")){
			printf("SUCCESSFUL\n");
			for(int i = 0; i < strlen(val); i++){
				char c[4] = "\'";
				if(val[i] == ' ') fprintf(fp, ":");
				else if(val[i] == c[0]) continue;
				else fprintf(fp, "%c", val[i]);
			}
			fprintf(fp, "\n");
			success[0] = '1'; success[1] = '\0';
			fclose(fp);
		} else {
			success[0] = '0'; success[1] = '\0';
			printf("TABLE DOESN'T EXIST!\n");
		}
		write(new_socket, success, 4);
	}
	else printf("WRONG INPUT\n");
}
```

> This code handles the insertion of data into a table. The function reads a mode string from the client through the new_socket and performs the insertion operation if the mode is "INTO". If the mode is "INTO," the function checks if a database is currently selected by checking the length of the use_db_path variable. If a database is not selected, it writes "1" to the client and returns an error message. Otherwise, it writes "0" to the client to indicate that the insertion operation can proceed. The function then reads the table name, value, and constructs the file path based on the selected database and the provided table name. It attempts to open the table file in "a" (append) mode. If the file is successfully opened, it iterates over each character in the provided value and writes it to the file. It skips writing spaces and single quotes (') to the file. Additionally, it inserts a colon (:) character as a delimiter between each value. Finally, it writes a newline character (\n) to indicate the end of the inserted row. If the table file is successfully opened and the insertion is performed, it writes "1" to the client and prints a success message. If the table file doesn't exist, it writes "0" to the client and prints an error message. If the provided mode is neither "INTO" nor recognized, an error message is printed.

```c
void sel(int new_socket){
	char arg[20][SET_SIZE] = {0};
	int n_arg = 0;;
	
	// ? Column
	read(new_socket, arg[n_arg], SET_SIZE);
	while(strcmp(arg[n_arg], "-")){
		printf("Take column loop\n");
		if(!strlen(arg[n_arg])) return;
		printf("%d %s\n", n_arg, arg[n_arg]);
		n_arg++;
		read(new_socket, arg[n_arg], SET_SIZE);
	}
	printf("End of select argument\n");

	// ? Table
	char table_name[SET_SIZE] = {0};
	read(new_socket, table_name, SET_SIZE);
	if(!strlen(table_name)) return;

	// ? Callin out "Where" if its needed
	char where_arg[SET_SIZE] ={0};
	if(!is_where(new_socket, where_arg)) return;

	// ? Checking User input
	// ? Error on "Use"
	if(!strlen(use_db_path)){
		printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
		write(new_socket, "ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n", 256);
		return;
	}
	write(new_socket, "\0", 256);

	char temp_path[SET_SIZE] = {0};
	strcpy(temp_path, use_db_path);
	strcat(temp_path, table_name);
	strcat(temp_path, ".txt");

	printf("%s\n", temp_path);
	FILE* fp;
	if(fp = fopen(temp_path, "r")){
		// ! Table Feedback
		printf("TABLE EXIST\n");
		write(new_socket, "\0", 256);

		// ! Column Feedback
		char col_name[SET_SIZE] = {0}, db[2048] = {0};
		if(!fgets(db, sizeof(db), fp)) printf("UNKNOWN ERROR\n");
		if(db[strlen(db)-1] == '\n') db[strlen(db)-1] == '\0';

		int i = 0, j = 0, k = 0, n_col = 0, recog = 0, col_position[n_arg], col_ke = 0;
		
		// ? Getting Inside the Column
		while(i < strlen(db)) { 
			if(i == 0 && !strcmp(arg[k], "*")) {
				printf("DISPLAY ALL\n");
				write(new_socket, "\0", 256);
				n_arg = -1;
				break;
			}
			printf("Masuk loop check kolom\n");
			j = 0;
			
			while(db[i] != ':' && db[i] != '\n') {
				col_name[j] = db[i]; j++; i++;
			}
			col_name[j] = '\0';
			i++;

			// ? Checking Column Existence
			for(k = 0; k < n_arg; k++) {
				printf("arg:%s\ncol_name: %s\n", arg[k], col_name);
				if(!strcmp(arg[k], col_name)) { 
					arg[k][0] = '\0'; col_position[k] = col_ke;
				}
			}
			col_ke++;
			int recog_all = 1;
			if(i == strlen(db)) {
				//send all the unrecog col to client
				for(k = 0; k < n_arg; k++) {
					if(strlen(arg[k])) {
						recog_all = 0;
						printf("Table '%s' not recognized: %d\n", arg[k], k);
						write(new_socket, arg[k], 256);
					}
				}
				write(new_socket, "\0", 256);
			}

			if(!recog_all) return;
		}
		printf("Total column in Database: %d\n%s\n", col_ke, db); 
		fgets(db, sizeof(db), fp);
		
		// ? Init var
		i = 0;
		j = 0;
		while(fgets(db, sizeof(db), fp)) {
			if(db[strlen(db)-1] == '\n') db[strlen(db)-1] = '\0';
			if(n_arg == -1) {
				write(new_socket, db, 2048);
				continue;
			}

			int delim = 0;
			j = 0;
			for(i = 0; i < strlen(db); i++) {
				col_name[j] = db[i];
				if(db[i] == ':' || i == strlen(db) - 1) {
					if(col_name[j] == ':') col_name[j] = '\0';
					else if(i == strlen(db) - 1) col_name[j + 1] = '\0';
					strcpy(arg[delim], col_name);
					delim++; j = -1;
				}
				j++;
			}

			char temp[2048] = {0};
			for(k = 0; k < n_arg; k++){
				if(k > 0) strcat(temp, ":");
				strcat(temp, arg[col_position[k]]);
			}

			printf("temp: %s\n", temp);
			write(new_socket, temp, 2048);

		}
		write(new_socket, "\0", 2048);
		fclose(fp);
	} else {
		write(new_socket, "ERROR: TABLE NOT FOUND\n", 256);
		printf("ERROR: TABLE NOT FOUND\n");
		return;
	}
}
```

> The sel() function handles the "SELECT" operation. It reads the column names and table name from the client and checks if a "WHERE" condition is provided. It verifies the user input and constructs the file path for the table. If the table file is successfully opened, it processes the column names and sends feedback to the client. It then reads the table data, processes it based on the requested columns, and sends it to the client. Finally, it closes the file and handles any errors encountered during the process. The code could be improved by adding error handling and input validation, as well as addressing potential security vulnerabilities.


```c
int is_where(int new_socket, char *where_arg){
	read(new_socket, where_arg, SET_SIZE);
	if(strlen(where_arg)){
		read(new_socket, where_arg, SET_SIZE);
		if(strlen(where_arg)){
			read(new_socket, where_arg, SET_SIZE);
			if(!strlen(where_arg)) return 0;
		}
		else return 0;
	}
	printf("Where com: %s\n", where_arg);
	return 1;
}
```

> The provided code snippet includes a function named is_where() in a C program, which checks whether a "WHERE" condition is provided by the client. The function takes two arguments: new_socket, which represents the socket for communication with the client, and where_arg, which is a pointer to a character array where the "WHERE" condition will be stored. The function reads data from the client using the read() function. It reads SET_SIZE number of characters into the where_arg array. If the length of where_arg is non-zero (indicating that data was successfully read), it reads additional SET_SIZE characters twice more. The purpose of this code is to ensure that the where_arg contains a non-empty string for the "WHERE" condition. After reading the data, the function prints the "WHERE" condition by using printf() to display the content of where_arg. Finally, the function returns either 1 or 0 based on whether a valid "WHERE" condition is provided. If the length of where_arg is non-zero, it returns 1, indicating that a "WHERE" condition is present. Otherwise, it returns 0, indicating that no "WHERE" condition is provided.

- [x] Inserts
- [x] Select
- [x] Where

![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/insertinto.jpg)
![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/afterinsert.jpg)
![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/select.jpg)
![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/where.jpg)

## Logging

```c
// ! Log
void printTimestamp(const char* username, const char* command) {
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);

    FILE* fp = open_file("/databases/log.txt");
    if (fp != NULL) {
        fprintf(fp, "%s:%s:%s\n", buffer, username, command);
        fclose(fp);
    } else {
        printf("Log file failed to open.\n");
    }
}
```
> The code above defines a function printTimestamp that logs the current timestamp, username, and command to a file named "log.txt". It retrieves the current time, formats it as a timestamp, and opens the log file. If the file is successfully opened, the log entry is written to the file using fprintf, including the timestamp, username, and command. If the file fails to open, an error message is displayed.

- [x] Log

![log proof](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/log.jpg)

**_NOTE : This was just some of the log not the updated one, cause we took it in the middle of coding_**

## Error Handling

> Error handling code are spread in many function, in that case we can't provide you all the code. But we can proof that our error handling is working using the picture below.

- [x] error handling

![](https://gitlab.com/sisop-praktikum-modul-1-2023-BJ-U08/sisop-praktikum-fp-2023-bj-u08/-/raw/main/img/error.jpg)

## Containerization

> Already explained above, in the containerization section

- [x] containerization

## Extra

> The client and dump programs have communicated with the socket to the main program and do not directly access the files in the main program folder, as you can see from the screenshot above.

- [x] extra

# **Problems Faced**

- Programs often got 'segementation fault'
- Susah mas mba, ngga sanggup T_T
