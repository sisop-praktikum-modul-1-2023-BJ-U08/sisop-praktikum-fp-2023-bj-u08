FROM ubuntu:latest

WORKDIR /app

COPY . /app

RUN apt-get update && apt-get install -y gcc

CMD gcc -o client ./client/client.c && \
    gcc -o database ./database/database.c && \
    ./client && \
    ./database && \
