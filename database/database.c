#include <sys/stat.h>
#include <pthread.h>
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <time.h>

// ! Make Sure it has the exact same port with database
#define PORT 8080
#define SET_SIZE 1024

char path[] = "/home/naufalihza/Jeje", use_db_path[SET_SIZE] = {0}, client[SET_SIZE] = {0};
int n_new_user = 0, n_new_permission = 0;

// ! Open File
FILE *open_file(char *file_path){
	FILE* fp;
	char temp_path[SET_SIZE] = {0};
	strcat(temp_path, path);
	strcat(temp_path, file_path);
	fp = fopen(temp_path, "a+");
	return fp;
}

// ! Log
void printTimestamp(const char* username, const char* command) {
    time_t rawtime;
    struct tm *timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);

    FILE* fp = open_file("/databases/log.txt");
    if (fp != NULL) {
        fprintf(fp, "%s:%s:%s\n", buffer, username, command);
        fclose(fp);
    } else {
        printf("Log file failed to open.\n");
    }
}

// ! Remove Directory
int remove_directory(const char *path) {
   DIR *d = opendir(path);
   size_t path_len = strlen(path);
   int r = -1;

   if (d) {
      struct dirent *p;

      r = 0;
      while (!r && (p=readdir(d))) {
          int r2 = -1;
          char *buf;
          size_t len;

          /* Skip the names "." and ".." as we don't want to recurse on them. */
          if (!strcmp(p->d_name, ".") || !strcmp(p->d_name, ".."))
             continue;

          len = path_len + strlen(p->d_name) + 2; 
          buf = malloc(len);

          if (buf) {
             struct stat statbuf;

             snprintf(buf, len, "%s/%s", path, p->d_name);
             if (!stat(buf, &statbuf)) {
                if (S_ISDIR(statbuf.st_mode))
                   r2 = remove_directory(buf);
                else
                   r2 = unlink(buf);
             }
             free(buf);
          }
          r = r2;
      }
      closedir(d);
   }

   if (!r)
      r = rmdir(path);

   return r;
}

// ! LOGIN
int login(int new_socket) {
	char username[SET_SIZE] = {0}, password[SET_SIZE] = {0};
	read(new_socket, username, SET_SIZE);	puts(username);
	read(new_socket, password, SET_SIZE);	puts(password);

	FILE* fp = open_file("/databases/users.txt");

	char temp[SET_SIZE] = {0}, db[SET_SIZE] = {0};
	int correct_pass = 0;
	strcat(temp, username);
	strcat(temp, ":");
	strcat(temp, password);
	while(fgets(db, sizeof(db), fp)){
		if(db[strlen(db)-1] == '\n') db[strlen(db)-1] = '\0';
		if(!strcmp(temp, db)) {
			correct_pass = 1;
			break;
		}
	}
	char status[200] = {0};
	if(correct_pass) {
		strcpy(status, "ACCESS GRANTED");
		strcpy(client, username);
	}
	else strcpy(status, "ACCESS DENIED");
	write(new_socket, status, 200);
	fclose(fp);
	if(strcmp(status, "ACCESS GRANTED") == 0) return 1;
	return 0;
}

// ! Availability
// ! DB
int is_db_avail(char *db_name) {
	char db_path[SET_SIZE] = {0};

	strcpy(db_path, path);
	strcat(db_path, "/databases/");
	strcat(db_path, db_name);

	DIR* db = opendir(db_path);
	//found
	if(db){
		printf("DATABASE FOUND\n");
		closedir(db);
		return 1;
	}

	//not found
	printf("ERROR: DATABASE NOT FOUND\n");
	closedir(db);
	return 0;
}

// ! USER
int is_usr_avail(char *new_user){
	FILE* fp = open_file("/databases/users.txt");
	char db[SET_SIZE] = {0};
	while(fgets(db, sizeof(db), fp)){
		n_new_user = 1;
		int i = 0;

		//compare each line
		while(i < strlen(db) && i < strlen(new_user)){
			printf("%c %c %d %lu\n", db[i], new_user[i], i, strlen(new_user));
			if(db[i] != new_user[i]) break;
			i++;
			if(db[i] == ':' && i == strlen(new_user)) {
				printf("yup\n");
				return 1;
			}
		}
	}
	return 0;
}

// ! PERMISSION
// ? 0 -> Permission don't exist or not created
// ? 1 -> Permission exist
// ? 2 -> Permission created
int permit(char *user, char *dir_name, int grant_permit){
	//grant_permit
	/// 0 -> Just check
	/// 1 -> Create permit
	FILE* fp = open_file("/databases/permissions.txt");
	char temp[SET_SIZE] = {0}, db[SET_SIZE] = {0};
	strcpy(temp, user);
	strcat(temp, ":");
	strcat(temp, dir_name);

	while(fgets(db, sizeof(db), fp)){
		n_new_permission = 1;
		if(db[strlen(db)-1] == '\n') db[strlen(db)-1] = '\0';

		//Permission exist
		printf("In permit, fgets function first loop\n");
		if(!strcmp(temp, db)) {	fclose(fp);return 1;}
	}
	if(grant_permit){
		//Create permission
		if(n_new_permission) {printf("%d print slash n\n", n_new_permission);fprintf(fp, "\n");}
		printf("user:database: %s\n", temp);
		fprintf(fp, "%s", temp);
		printf("PERMISSION CREATED\n");
		n_new_permission++;

		fclose(fp);
		//Permission created
		return 2;
	}
	fclose(fp);
	//Permission don't exist or not created
	return 0;
}

// ! ROOT GRANT PERMISSION
void root_grant_permit(int new_socket){
	char db_name[SET_SIZE] = {0}, username[SET_SIZE] = {0};
	read(new_socket, db_name, SET_SIZE); if(!strlen(db_name)) {printf("Error dir read ke 1 root_grant\n"); return;}
	read(new_socket, username, SET_SIZE); if(!strlen(username)) {printf("Error di read read ke-2 root_grant\n");return;}

	//check db avail
	if(!is_db_avail(db_name)) {write(new_socket, "\0", 64); return;}
	else write(new_socket, "DATABASE FOUND", 64);

	//check user avail
	if(!is_usr_avail(username)) {write(new_socket, "\0", 64); return;}
	else write(new_socket, "USER FOUND", 64);

	int status = permit(username, db_name, 1);

	/// 1 -> Permission exist
	if(status == 1) {write(new_socket, "\0", 64); return;}

	/// 2 -> Permission created
	else if(status == 2) write(new_socket, "PERMISSION GRANTED\n", 64);
}

// ! Use
void use(int new_socket){
	//delete previous path
	use_db_path[0] = '\0';

	char db_name[SET_SIZE] = {0};
	read(new_socket, db_name, SET_SIZE);
	if(!strlen(db_name)) printf("ERROR: IN USE FUNCTION\n");

	//if all going smoothly, send empty string

	//is db avail?
	if(!is_db_avail(db_name)) {
		printf("ERROR: DB NOT AVAIL\n");
		write(new_socket, "ERROR: DATABASE NOT FOUND", 64);
		return;
	}

	//is client root?
	if(!strlen(client)) {
		//client is root
		printf("CLIENT IS ROOT\n");
	}

	//if client not roor -> is permission avail?
	else if(!permit(client, db_name, 0)){
		printf("ERROR: PERMIT NOT AVAILABLE\n");
		write(new_socket, "ERROR: YOU DON'T HAVE PERMISSION TO ACCESS THIS DATABASE", 64);
		return;
	}

	strcpy(use_db_path, path);
	strcat(use_db_path, "/databases/");
	strcat(use_db_path, db_name);
	strcat(use_db_path, "/");

	printf("%s\n", use_db_path);
	write(new_socket, "\0", 64);
}

// ! Create New
void create(int new_socket){
	printf("Masuk create\n");
	char mode[64] = {0};
	read(new_socket, mode, 64);
	
	// ! User
	if(!strcmp(mode, "USER") && !strlen(client)) {
		char new_user[SET_SIZE] = {0}, new_pwd[SET_SIZE] = {0};
		read(new_socket, new_user, SET_SIZE);
		if(!strlen(new_user)) {
			printf("NEW USER WRONG INPUT\n");
			return;
		}
		read(new_socket, new_pwd, SET_SIZE);
		if(!strlen(new_pwd)) {
			printf("NEW PWD WRONG INPUT\n");
			return;
		}

		char temp[SET_SIZE] = {0};

		// ? If username avail
		if(is_usr_avail(new_user)) write(new_socket, "0", 4);

		// ? Username not avail
		else write(new_socket, "1", 4);
		FILE* fp = open_file("/databases/users.txt");

		// ? Store username and password
		strcpy(temp, new_user);
		strcat(temp, ":");
		strcat(temp, new_pwd);

		if(n_new_user) {
			printf("%d print slash n\n", n_new_user);
			fprintf(fp, "\n");
		}

		printf("%s\n", temp);
		fprintf(fp, "%s", temp);
		printf("USER CREATED\n");
		printTimestamp("root", "USER CREATED");

		n_new_user++;
		fclose(fp);
	}

	// ! DATABASE
	else if(!strcmp(mode, "DATABASE")) {
		char dir_name[512] = {0}, new_user[SET_SIZE] = {0}, temp_path[SET_SIZE] = {0}, success[4] ={0};
		read(new_socket, dir_name, SET_SIZE);
		if(!strlen(dir_name)) {
			printf("DATABASE WRONG INPUT\n");
			return;
		}

		strcpy(temp_path, path);
		strcat(temp_path, "/databases/");
		strcat(temp_path, dir_name);
		if(!mkdir(temp_path, S_IRWXU)) {
			success[0] = '1'; success[1] = '\0';
			printf("DATABASE CREATED\n");
			if(strlen(client)) permit(client, dir_name, 1);
			
			printTimestamp("root", "DATABASE CREATED");
		} else {
			success[0] = '0';
			success[1] = '\0';
			printf("NAME EXIST");
		}
		write(new_socket, success, 4);
	}

	// ! TABLE
	else if(!strcmp(mode, "TABLE")) {
        	char success[4] ={0}, new_user[SET_SIZE] = {0};
        	if(!strlen(use_db_path)){
			printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
			success[0] = '3'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
		}
		char dir_name[512] = {0}, temp_path[SET_SIZE] = {0}, util[SET_SIZE] = {0};
		read(new_socket, dir_name, SET_SIZE);
		if(!strlen(dir_name)) {
			printf("TABLE WRONG INPUT\n");
			return;
		}
		strcpy(temp_path, use_db_path);
		strcat(temp_path, dir_name);
		strcat(temp_path, ".txt");
		read(new_socket, util, SET_SIZE);
		printf("util : %s\n", util);
		char col[SET_SIZE]= {0}, datatype[SET_SIZE] = {0}, temp_datatype[SET_SIZE] = {0};
		int type = 2, total = strlen(util);
		for(int k = 0; k < total; k++) {
			if(util[k] == ' ') {
				char c[2];
				c[1] = '\0'; c[0] = ':';
				if(type % 2 == 0) strcat(col, c);
				else {
					if(!strcmp(temp_datatype, "string") || !strcmp(temp_datatype, "int")){
						strcat(datatype, temp_datatype);
						memset(temp_datatype, 0, sizeof(temp_datatype));
						strcat(datatype, c);
					}
					else{
						success[0] = '2'; success[1] = '\0';
						printf("FAIL, WRONG DATATYPE\n");
						write(new_socket, success, 4);
						return;
					}
				}
				type += 1;
			}
			else{
				char c[2];
				c[1] = '\0'; c[0] = util[k];
				if(type % 2 == 0) strcat(col, c);
				else strcat(temp_datatype, c);
			}
		}
		total = strlen(col);
		strcat(datatype, temp_datatype);
		col[total-1] = '\0';
		FILE* fp;
		if(fp = fopen(temp_path, "r")){
			success[0] = '0'; success[1] = '\0';
			fclose(fp);
			printf("TABLE EXIST");
		}
		else{
			success[0] = '1'; success[1] = '\0';
			fp = fopen(temp_path, "a");
			fprintf(fp,"%s\n%s\n",col, datatype);
			fclose(fp);
			printf("TABLE CREATED\n");
			
			printTimestamp("root", "TABLE CREATED");
		}
		write(new_socket, success, 4);
	}
	else printf("ERROR: WRONG INPUT\n");
}

// ! Drop
void drop(int new_socket){
	char mode[512] = {0};
	read(new_socket, mode, SET_SIZE);
	
	// ! Database
	if(!strcmp(mode, "DATABASE")) {
		char dir_name[512] = {0};
		read(new_socket, dir_name, SET_SIZE);
		if(!strlen(dir_name)) {printf("DATABASE WRONG INPUT\n"); return;}
		char temp_path[SET_SIZE] = {0};
		strcpy(temp_path, path);
		strcat(temp_path, "/databases/");
		strcat(temp_path, dir_name);
		char success[4] ={0};
		if(!strlen(client)) printf("CLIENT IS ROOT\n");
		else if(!permit(client, dir_name, 0)) {
			printf("ERROR: PERMIT NOT AVAILABLE\n");
			success[0] = '2'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
		}
		DIR* dir = opendir(temp_path);
		if(dir) {
			remove_directory(temp_path);
			success[0] = '1'; success[1] = '\0';
			printf("DATABASE REMOVED\n");
			printTimestamp("root", "DATABASE REMOVED");
		} else {
			success[0] = '0'; success[1] = '\0';
			printf("DATABASE DO NOT EXIST");
		}
		write(new_socket, success, 4);
	}

	// ! Table
	else if(!strcmp(mode, "TABLE")) {
		char dir_name[SET_SIZE] = {0};
		read(new_socket, dir_name, SET_SIZE);
		char success[4] ={0};
		if(!strlen(use_db_path)){
			printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
			success[0] = '3'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
        	}
		if(!strlen(dir_name)) {
			printf("TABLE WRONG INPUT\n");
			return;
		}
		char temp_path[SET_SIZE] = {0};
		strcpy(temp_path, use_db_path);
		strcat(temp_path, dir_name);
		strcat(temp_path, ".txt");
		FILE* fp;
		if(fp = fopen(temp_path, "r")) {
			if(!remove(temp_path)) {
				success[0] = '1'; success[1] = '\0';
				printf("TABLE SUCCESSFULLY REMOVED");
				printTimestamp("root", "TABLE REMOVED");
			} else {
				success[0] = '2'; success[1] = '\0';
				printf("TABLE IS NOT REMOVED");
			}
            		fclose(fp);
		} else {
			success[0] = '0'; success[1] = '\0';
            		fclose(fp);
            		printf("TABLE DOESN'T EXIST");
		}
		write(new_socket, success, 4);
	}

	// ! Column
	else if(!strcmp(mode, "COLUMN")){
		if(!strlen(use_db_path)) {
		    printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
		    return;
        	}
		char dir_name[512] = {0}, success[4] = {0}, col_name[512] = {0}, table_name[512] = {0};
		
		read(new_socket, col_name, SET_SIZE);
		printf("col_name : %s\n", col_name);
		read(new_socket, table_name, SET_SIZE);
		printf("table_name : %s\n", table_name);
		
		char temp_path[SET_SIZE] = {0};
		
		strcpy(temp_path, use_db_path);
		strcat(temp_path, table_name);
		strcat(temp_path, ".txt");
		printf("%s\n", temp_path);
		FILE* fp = fopen(temp_path, "r");
		if(!fp){
			printf("TABLE DOESN'T EXIST\n");
			success[0] = '0'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
		}
		char db[8192] = {0};
		fgets(db, sizeof(db), fp);
		printf("%s", db);
		char col[512] = {0}, total_fill[4096] = {0};
		int no_col = 1, temp_col = 1, bisa = 0;
		for(int i = 0; i < strlen(db); i++){
			char c[2];
			c[1] = '\0';
			if(db[i] == ':'){
				printf("%s\n", col);
				if(!strcmp(col,col_name)){
					bisa = 1;
					break;
				}
				else{
					memset(col, 0, sizeof(col));
					no_col += 1;
				}
			}
			else{
				c[0] = db[i];
				strcat(col, c);
			}
		}
		printf("%d no_col\n", no_col);
		if(!bisa){
			printf("COLUMN DOESN'T EXIST\n");
			success[0] = '2'; success[1] = '\0';
			write(new_socket, success, 4);
			return;
		}
		char c[2];
		c[1] = '\0';
		printf("%lu strlendb\n", strlen(db));
		for(int i = 0; i < strlen(db); i++){
			if(temp_col == no_col) {
				if(db[i] == ':') {
					temp_col += 1;
				}
				continue;
			} else {
				if(db[i] == ':') {
					temp_col += 1;
				}
				c[0] = db[i];
				strcat(total_fill, c);
			}
		}
		printf("total_fill : %s\n", total_fill);
		while(fgets(db, sizeof(db), fp)) {
			temp_col = 1;
			for(int i = 0; i < strlen(db); i++) {
				char c[2];
				c[1] = '\0';
				if(temp_col == no_col) {
					if(db[i] == ':') temp_col += 1;
					continue;
				} else {
					if(db[i] == ':') temp_col += 1;
					c[0] = db[i];
					strcat(total_fill, c);
				}
			}
		}
		printf("total_fill 2nd : %s\n", total_fill);
		fp = fopen(temp_path, "w");
		fprintf(fp, "%s", total_fill);
		fclose(fp);
		success[0] = '1'; success[1] = '\0';
		write(new_socket, success, 4);
	}
	else printf("WRONG INPUT\n");
}

// ! Data Manipulation Language
// ! Insert
// ? Format : INSERT INTO [nama_tabel] ([value], ...);
void insert(int new_socket){
	char mode[512] = {0};
	char success[4] = {0};
	char avail[4] = {0};
	read(new_socket, mode, 512);
	if(!strcmp(mode, "INTO")){
		if(!strlen(use_db_path)){
			printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
			avail[0] = '1'; avail[1] = '\0';
		} else {
			avail[0] = '0'; avail[1] = '\0';
		}
		write(new_socket, avail, 4);
		if(avail[0] == '1') return;
		char dir_name[SET_SIZE], temp_path[SET_SIZE];
		read(new_socket, dir_name, SET_SIZE);
		printf("%s\n", dir_name);
		char val[SET_SIZE];
		read(new_socket, val, SET_SIZE);
		printf("%s\n", val);
		strcpy(temp_path, use_db_path);
		strcat(temp_path, dir_name);
		strcat(temp_path, ".txt");
		printf("%s\n", temp_path);
		
		FILE* fp;
		if (fp = fopen(temp_path, "a")){
			printf("SUCCESSFUL\n");
			for(int i = 0; i < strlen(val); i++){
				char c[4] = "\'";
				if(val[i] == ' ') fprintf(fp, ":");
				else if(val[i] == c[0]) continue;
				else fprintf(fp, "%c", val[i]);
			}
			fprintf(fp, "\n");
			success[0] = '1'; success[1] = '\0';
			fclose(fp);
		} else {
			success[0] = '0'; success[1] = '\0';
			printf("TABLE DOESN'T EXIST!\n");
		}
		write(new_socket, success, 4);
	}
	else printf("WRONG INPUT\n");
}


// ! Data Manipulation Language
// ! Where
// ? Format : ... where [nama_kolom]=[value];
int is_where(int new_socket, char *where_arg){
	read(new_socket, where_arg, SET_SIZE);
	if(strlen(where_arg)){
		read(new_socket, where_arg, SET_SIZE);
		if(strlen(where_arg)){
			read(new_socket, where_arg, SET_SIZE);
			if(!strlen(where_arg)) return 0;
		}
		else return 0;
	}
	printf("Where com: %s\n", where_arg);
	return 1;
}

// ! Data Manipulation Language
// ? Select
// ? Format : SELECT [nama_kolom, … | *] FROM [nama_tabel];
void sel(int new_socket){
	char arg[20][SET_SIZE] = {0};
	int n_arg = 0;;
	
	// ? Column
	read(new_socket, arg[n_arg], SET_SIZE);
	while(strcmp(arg[n_arg], "-")){
		printf("Take column loop\n");
		if(!strlen(arg[n_arg])) return;
		printf("%d %s\n", n_arg, arg[n_arg]);
		n_arg++;
		read(new_socket, arg[n_arg], SET_SIZE);
	}
	printf("End of select argument\n");

	// ? Table
	char table_name[SET_SIZE] = {0};
	read(new_socket, table_name, SET_SIZE);
	if(!strlen(table_name)) return;

	// ? Callin out "Where" if its needed
	char where_arg[SET_SIZE] ={0};
	if(!is_where(new_socket, where_arg)) return;

	// ? Checking User input
	// ? Error on "Use"
	if(!strlen(use_db_path)){
		printf("ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n");
		write(new_socket, "ERROR: PLEASE INPUT \"USE\" COMMAND TO SELECT DATABASE\n", 256);
		return;
	}
	write(new_socket, "\0", 256);

	char temp_path[SET_SIZE] = {0};
	strcpy(temp_path, use_db_path);
	strcat(temp_path, table_name);
	strcat(temp_path, ".txt");

	printf("%s\n", temp_path);
	FILE* fp;
	if(fp = fopen(temp_path, "r")){
		// ! Table Feedback
		printf("TABLE EXIST\n");
		write(new_socket, "\0", 256);

		// ! Column Feedback
		char col_name[SET_SIZE] = {0}, db[2048] = {0};
		if(!fgets(db, sizeof(db), fp)) printf("UNKNOWN ERROR\n");
		if(db[strlen(db)-1] == '\n') db[strlen(db)-1] == '\0';

		int i = 0, j = 0, k = 0, n_col = 0, recog = 0, col_position[n_arg], col_ke = 0;
		
		// ? Getting Inside the Column
		while(i < strlen(db)) { 
			if(i == 0 && !strcmp(arg[k], "*")) {
				printf("DISPLAY ALL\n");
				write(new_socket, "\0", 256);
				n_arg = -1;
				break;
			}
			printf("Masuk loop check kolom\n");
			j = 0;
			
			while(db[i] != ':' && db[i] != '\n') {
				col_name[j] = db[i]; j++; i++;
			}
			col_name[j] = '\0';
			i++;

			// ? Checking Column Existence
			for(k = 0; k < n_arg; k++) {
				printf("arg:%s\ncol_name: %s\n", arg[k], col_name);
				if(!strcmp(arg[k], col_name)) { 
					arg[k][0] = '\0'; col_position[k] = col_ke;
				}
			}
			col_ke++;
			int recog_all = 1;
			if(i == strlen(db)) {
				//send all the unrecog col to client
				for(k = 0; k < n_arg; k++) {
					if(strlen(arg[k])) {
						recog_all = 0;
						printf("Table '%s' not recognized: %d\n", arg[k], k);
						write(new_socket, arg[k], 256);
					}
				}
				write(new_socket, "\0", 256);
			}

			if(!recog_all) return;
		}
		printf("Total column in Database: %d\n%s\n", col_ke, db); 
		fgets(db, sizeof(db), fp);
		
		// ? Init var
		i = 0;
		j = 0;
		while(fgets(db, sizeof(db), fp)) {
			if(db[strlen(db)-1] == '\n') db[strlen(db)-1] = '\0';
			if(n_arg == -1) {
				write(new_socket, db, 2048);
				continue;
			}

			int delim = 0;
			j = 0;
			for(i = 0; i < strlen(db); i++) {
				col_name[j] = db[i];
				if(db[i] == ':' || i == strlen(db) - 1) {
					if(col_name[j] == ':') col_name[j] = '\0';
					else if(i == strlen(db) - 1) col_name[j + 1] = '\0';
					strcpy(arg[delim], col_name);
					delim++; j = -1;
				}
				j++;
			}

			char temp[2048] = {0};
			for(k = 0; k < n_arg; k++){
				if(k > 0) strcat(temp, ":");
				strcat(temp, arg[col_position[k]]);
			}

			printf("temp: %s\n", temp);
			write(new_socket, temp, 2048);

		}
		write(new_socket, "\0", 2048);
		fclose(fp);
	} else {
		write(new_socket, "ERROR: TABLE NOT FOUND\n", 256);
		printf("ERROR: TABLE NOT FOUND\n");
		return;
	}
}

// ! Handling Multiple User
// ? Only if there are more than one client using the database
void *connection_control(void *new_sock){
	int new_socket = *(int*) new_sock;
	write(new_socket, "Go", 4);

	client[0] = '\0';
	use_db_path[0] = '\0';
	char root[4] = {0};
	read(new_socket, root, 4);
	if(!strcmp(root, "1") || login(new_socket)) {
		printf("Client Login\n");
		while(1){
			char com[SET_SIZE] = {0};
			read(new_socket, com, SET_SIZE);
			if(!strcmp(com, "CREATE")) create(new_socket);
			else if(!strcmp(com, "USE")) use(new_socket);
			else if(!strcmp(com, "GRANT")) root_grant_permit(new_socket);
			else if(!strcmp(com, "DROP")) drop(new_socket);
			else if(!strcmp(com, "INSERT")) insert(new_socket);
			else if(!strcmp(com, "SELECT")) sel(new_socket);
			else if(!strcmp(com, "exit")) {printf("See you later!\n"); break;}
			else {printf("Wrong input in menu\n"); continue;}
		}
	}
}

int main(int argc, char const *argv[]) {
	int server_fd, new_socket, valread;
	struct sockaddr_in address;
	int opt = 1, addrlen = sizeof(address);

	// ? Socket
	if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_addr.s_addr = INADDR_ANY;
	address.sin_port = htons( PORT );

	if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	if (listen(server_fd, 1) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	char temp[SET_SIZE] = {0};
	strcpy(temp, path);
	strcat(temp, "/databases/");
	if(!mkdir(temp, S_IRWXU)){
		strcat(temp, "/root_db");
		mkdir(temp, S_IRWXU);
	}

	// Connect With Client
	puts("Waiting for client...");
	while ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))) {
		printf("Connected to client\n");
		pthread_t client_thread;
		int *new_sock = malloc(1);
		*new_sock = new_socket;
		if(pthread_create(&client_thread, NULL, connection_control, (void*) new_sock) < 0) {
			perror("Could not create thread");
			return 1;
		}
		pthread_join(client_thread, NULL);
		puts("Disconnected");
		puts("Waiting for client");
	}
	if(new_socket < 0){
		perror("accept");
		exit(EXIT_FAILURE);
	}

	return 0;
}
